import shutil
import sys, os
import time
from distutils.dir_util import copy_tree
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import Qt
import sqlite3
from PIL import Image

import drawBoxUpdate
import updateData
import pickle
from pathlib import Path
from storage import storeResultsWarning

con = sqlite3.connect("images2.db")
cur = con.cursor()

fixPos = []
folderPath = ""
results = []
resultsWarning = []
root = Path(".")


def loadVariables():
    global fixPos
    global folderPath
    global results
    global resultsWarning

    # load fixPos
    inputFile = 'fixPos.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    fixPos = pickle.load(fd)

    # load folderPath
    inputFile = 'folderPath.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    folderPath = pickle.load(fd)

    # load results
    inputFile = 'results.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    results = pickle.load(fd)

    # load warningBox
    inputFile = 'resultsWarning.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    resultsWarning = pickle.load(fd)
def copyResults():
    # save detected image to destination folder
    current_dir = os.getcwd()
    des_dir = current_dir + "/result_multi_update/" + os.path.basename(folderPath)
    src_dir = current_dir + "/result_multi/" + os.path.basename(folderPath)
    # create folder
    if os.path.exists(des_dir):
        shutil.rmtree(des_dir)
        time.sleep(0.1)
    copy_tree(src_dir,des_dir)

class Main(QMainWindow):

    def __init__(self):
        super().__init__()
        self.setWindowTitle("INDA Detection")
        self.setWindowIcon(QIcon('icons/icon3.ico'))
        self.setGeometry(50, 70, 1920, 1080)
        self.showMaximized()
        self.UI()
        self.show()

    def UI(self):
        self.toolBar()
        self.tabWigdet()
        self.widgets()
        self.layouts()
        self.displayImages()

    # 3 buttons on top
    def toolBar(self):
        self.tb = self.addToolBar("Tool Bar")
        self.tb.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        #####################Toolbar Buttons############
        ####################Open single Data ################
        self.addImage = QAction(QIcon('icons/add.png'), "Open single Data", self)
        self.tb.addAction(self.addImage)
        self.addImage.triggered.connect(self.openSingleData)
        self.tb.addSeparator()
        ######################Open multiple Data################
        self.addImages = QAction(QIcon('icons/add.png'), "Open multile Data", self)
        self.tb.addAction(self.addImages)
        self.addImages.triggered.connect(self.openMultipleData)
        self.tb.addSeparator()

    # tab widget
    def tabWigdet(self):
        self.tabs = QTabWidget()
        self.tabs.blockSignals(True)
        self.tabs.currentChanged.connect(self.tabChanged)
        self.setCentralWidget(self.tabs)
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()
        self.tabs.addTab(self.tab1, "Normal Detection")
        self.tabs.addTab(self.tab2, "Detection < 96")
        self.tabs.addTab(self.tab3, "Detection Left/Right")

    def widgets(self):
        #######################Tab1 Widgets###############
        ####################Main left layout widget##########
        self.itemsTable = QTableWidget()
        self.itemsTable.setColumnCount(6)

        self.itemsTable.setHorizontalHeaderItem(0, QTableWidgetItem("image_filename"))
        self.itemsTable.setHorizontalHeaderItem(1, QTableWidgetItem("temperature"))
        self.itemsTable.setHorizontalHeaderItem(2, QTableWidgetItem("frozen_total"))
        self.itemsTable.setHorizontalHeaderItem(3, QTableWidgetItem("f_ice"))
        self.itemsTable.setHorizontalHeaderItem(4, QTableWidgetItem("frozen_new"))
        self.itemsTable.setHorizontalHeaderItem(5, QTableWidgetItem("frozen_new_ID"))

        self.itemsTable.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
        self.itemsTable.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)
        self.itemsTable.horizontalHeader().setSectionResizeMode(2, QHeaderView.Stretch)
        self.itemsTable.horizontalHeader().setSectionResizeMode(3, QHeaderView.Stretch)
        self.itemsTable.horizontalHeader().setSectionResizeMode(4, QHeaderView.Stretch)
        self.itemsTable.horizontalHeader().setSectionResizeMode(5, QHeaderView.ResizeToContents)

        self.itemsTable.doubleClicked.connect(self.selectedItem)

        ########################Right top layout widgets#######################
        self.searchText = QLabel("Search")
        self.searchEntry = QLineEdit()
        self.searchEntry.setPlaceholderText("Search For Data")
        self.searchButton = QPushButton("Search")
        self.searchButton.clicked.connect(self.searchItems)

        ##########################Right middle layout widgets###########
        self.allItems = QRadioButton("All")
        self.onlyNewFrozenItems = QRadioButton("Only New Frozen")
        self.listButton = QPushButton("List")
        self.listButton.clicked.connect(self.listItems)

        ##########################Right bottom layout widgets###########
        self.BrowserDetectedDataButton = QPushButton("Open Detected Folder")
        self.BrowserDetectedDataButton.clicked.connect(self.openDetectedFolder)

        ##########################Right bottom2 layout widgets###########
        # TODO
        self.text1 = QLabel("Information/Notification will be shown here!")
        self.text1.setAlignment(Qt.AlignTop)

    def layouts(self):
        ######################Tab1 layouts##############
        self.mainLayout = QHBoxLayout()
        self.mainLeftLayout = QVBoxLayout()
        self.mainRightLayout = QVBoxLayout()
        self.rightTopLayout = QHBoxLayout()
        self.rightMiddleLayout = QHBoxLayout()
        self.rightBottomLayout = QHBoxLayout()
        self.rightBottomLayout2 = QHBoxLayout()

        self.topGroupBox = QGroupBox("Search Detected Data")
        self.middleGroupBox = QGroupBox("List Detected Data")
        self.bottomGroupBox = QGroupBox()
        self.bottomGroupBox2 = QGroupBox("Information Log")

        #################Add widgets###################
        ################Left main layout widget###########
        self.mainLeftLayout.addWidget(self.itemsTable)

        ########################Right top layout widgets#########
        self.rightTopLayout.addWidget(self.searchText)
        self.rightTopLayout.addWidget(self.searchEntry)
        self.rightTopLayout.addWidget(self.searchButton)
        self.topGroupBox.setLayout(self.rightTopLayout)

        #################Right middle layout widgets##########
        self.rightMiddleLayout.addWidget(self.allItems)
        self.rightMiddleLayout.addWidget(self.onlyNewFrozenItems)
        self.rightMiddleLayout.addWidget(self.listButton)
        self.middleGroupBox.setLayout(self.rightMiddleLayout)

        #################Right bottom layout widgets##########
        self.rightBottomLayout.addWidget(self.BrowserDetectedDataButton)
        self.rightBottomLayout.addStretch()
        self.bottomGroupBox.setLayout(self.rightBottomLayout)

        #################Right bottom2 layout widgets##########
        self.rightBottomLayout2.addWidget(self.text1)
        self.bottomGroupBox2.setLayout(self.rightBottomLayout2)

        self.mainRightLayout.addWidget(self.topGroupBox, 20)
        self.mainRightLayout.addWidget(self.middleGroupBox, 20)
        self.mainRightLayout.addWidget(self.bottomGroupBox, 10)
        self.mainRightLayout.addWidget(self.bottomGroupBox2, 50)
        self.mainLayout.addLayout(self.mainLeftLayout, 70)
        self.mainLayout.addLayout(self.mainRightLayout, 30)
        self.tab1.setLayout(self.mainLayout)

    def openSingleData(self):
        os.system('python gui-yolo-3.py')

    def openMultipleData(self):
        os.system('python gui-yolo-3-multi.py')

    def displayImages(self):
        self.itemsTable.setFont(QFont("Times", 12))
        for i in reversed(range(self.itemsTable.rowCount())):
            self.itemsTable.removeRow(i)

        query = cur.execute("SELECT image_filename,temperature,frozen_total,f_ice,frozen_new,frozen_new_ID FROM t")
        for row_data in query:
            row_number = self.itemsTable.rowCount()
            self.itemsTable.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.itemsTable.setItem(row_number, column_number, QTableWidgetItem(str(data)))

        self.itemsTable.setEditTriggers(QAbstractItemView.NoEditTriggers)

    def searchItems(self):
        value = self.searchEntry.text()
        if value == "":
            QMessageBox.information(self, "Warning", "Search query cant be empty!!!")

        else:
            self.searchEntry.setText("")

            query = (
                "SELECT image_filename,temperature,frozen_total,f_ice,frozen_new,frozen_new_ID FROM t WHERE image_filename LIKE ? or frozen_new_ID LIKE ?")
            results = cur.execute(query, ('%' + value + '%', '%' + value + '%')).fetchall()
            print(results)

            if results == []:
                QMessageBox.information(self, "Warning", "There is no such a item ")

            else:
                for i in reversed(range(self.itemsTable.rowCount())):
                    self.itemsTable.removeRow(i)

                for row_data in results:
                    row_number = self.itemsTable.rowCount()
                    self.itemsTable.insertRow(row_number)
                    for column_number, data in enumerate(row_data):
                        self.itemsTable.setItem(row_number, column_number, QTableWidgetItem(str(data)))

    def listItems(self):
        if self.allItems.isChecked() == True:
            self.displayImages()
        elif self.onlyNewFrozenItems.isChecked():
            query = (
                "SELECT image_filename,temperature,frozen_total,f_ice,frozen_new,frozen_new_ID FROM t WHERE frozen_new_ID <>'[]'")
            items = cur.execute(query).fetchall()
            print(items)

            for i in reversed(range(self.itemsTable.rowCount())):
                self.itemsTable.removeRow(i)

            for row_data in items:
                row_number = self.itemsTable.rowCount()
                self.itemsTable.insertRow(row_number)
                for column_number, data in enumerate(row_data):
                    self.itemsTable.setItem(row_number, column_number, QTableWidgetItem(str(data)))

    def openDetectedFolder(self):
        global des_dir
        global folder_name
        query = ("SELECT image_filename FROM t")
        item = cur.execute(query).fetchone()
        item = str(item)
        item = item[2:]
        item = item[:-3]
        folder_name = item.split("_")
        print(folder_name[1])
        src_dir = os.getcwd()
        des_dir = src_dir + "/result_multi/" + folder_name[1]
        os.startfile(des_dir)

    def tabChanged(self):
        self.displayImages()

    ################################################################################################
    def selectedItem(self):
        global imageID
        listItem = []
        for i in range(0, 6):
            listItem.append(self.itemsTable.item(self.itemsTable.currentRow(), i).text())

        imageID = listItem[0]
        print(imageID)
        self.display = DisplayImage()
        self.display.show()


class DisplayImage(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Data Details")
        self.setWindowIcon(QIcon('icons/icon3.ico'))
        self.setGeometry(0, 0, 1920, 1080)
        self.showMaximized()
        # self.setFixedSize(1920,1000)
        self.UI()
        self.show()

    def UI(self):
        self.imageDetails()
        self.mainDesign()
        self.layouts()
        self.getImages()
        self.displayFirstRecord()

    def mainDesign(self):
        self.item_Img = QLabel()
        self.item_Img.setFixedSize(1280, 960)

        self.imageList = QListWidget()
        self.imageList.itemClicked.connect(self.singleClick)
        self.btnUpdate = QPushButton("Update")
        self.btnUpdate.clicked.connect(self.updateImage)

    def layouts(self):
        ###################Layouts###############
        self.mainLayout = QHBoxLayout()
        self.leftMainLayout = QHBoxLayout()
        self.leftLeftLayout = QHBoxLayout()
        self.leftRightLayout = QFormLayout()
        self.rightMainLayout = QVBoxLayout()
        self.rightTopLayout = QHBoxLayout()

        self.hbox = QHBoxLayout()

        #####################Adding child layouts to main layout###########
        self.rightMainLayout.addLayout(self.rightTopLayout)
        self.leftMainLayout.addLayout(self.leftLeftLayout)
        self.leftMainLayout.addLayout(self.leftRightLayout)
        self.mainLayout.addLayout(self.leftMainLayout, 90)
        self.mainLayout.addLayout(self.rightMainLayout, 10)
        ###################adding wigdets to layouts#################
        self.rightTopLayout.addWidget(self.imageList)
        self.rightTopLayout.addStretch()

        self.hbox.addStretch()
        self.hbox.addWidget(self.btnUpdate)
        self.hbox.addStretch()

        self.leftLeftLayout.addWidget(self.item_Img)
        self.leftLeftLayout.addStretch()
        ##############setting main window layout#####################
        self.setLayout(self.mainLayout)

    def getImages(self):
        query = "SELECT image_filename FROM t"
        images = cur.execute(query).fetchall()
        for image in images:
            image_str = str(image)
            image_str = image_str[2:]
            image_str = image_str[:-3]
            self.imageList.addItem(image_str)
        # Set current Row
        row = imageID.split("_")
        row_str = row[-1]
        row_str = row_str[:-4]
        row_int = int(row_str)
        self.imageList.setCurrentRow(row_int)

    def imageDetails(self):
        global imageID
        query = ("SELECT * FROM t WHERE image_filename = ?")
        items = cur.execute(query, (imageID,)).fetchone()
        self.image_filename = items[0]
        self.temperature = items[1]
        self.frozen_total = items[2]
        self.f_ice = items[3]
        self.frozen_new = items[4]
        self.frozen_new_ID = items[5]

    def displayFirstRecord(self):
        # Display Image
        src_dir = os.getcwd()
        des_dir = src_dir + "/result_multi_update/" + os.path.basename(folderPath)
        self.img = QPixmap(des_dir + '/' + self.image_filename)
        self.item_Img.setPixmap(self.img)
        # get mouse pos
        self.item_Img.mousePressEvent = self.getPos

        # Display Infos
        temperature = QLabel(self.temperature)
        frozen_total = QLabel(self.frozen_total)
        f_ice = QLabel(self.f_ice)
        frozen_new = QLabel(self.frozen_new)
        frozen_new_ID = QLabel(self.frozen_new_ID)

        self.leftRightLayout.setVerticalSpacing(20)
        self.leftRightLayout.addRow("temperature: ", temperature)
        self.leftRightLayout.addRow("frozen_total:", frozen_total)
        self.leftRightLayout.addRow("f_ice:", f_ice)
        self.leftRightLayout.addRow("frozen_new:", frozen_new)
        self.leftRightLayout.addRow("frozen_new_ID:", frozen_new_ID)
        self.leftRightLayout.addRow(self.hbox)

    def singleClick(self):
        loadVariables()
        for i in reversed(range(self.leftRightLayout.count())):
            widget = self.leftRightLayout.takeAt(i).widget()

            if widget is not None:
                widget.deleteLater()

        item = self.imageList.currentItem().text()
        print(item)
        query = ("SELECT * FROM t WHERE image_filename = ?")
        items = cur.execute(query, (item,)).fetchone()
        self.image_filename = items[0]
        self.temperature = items[1]
        self.frozen_total = items[2]
        self.f_ice = items[3]
        self.frozen_new = items[4]
        self.frozen_new_ID = items[5]

        # Display Image
        src_dir = os.getcwd()
        des_dir = src_dir + "/result_multi_update/" + os.path.basename(folderPath)
        self.img = QPixmap(des_dir + '/' + self.image_filename)
        self.item_Img.setPixmap(self.img)

        # Display Infos
        temperature = QLabel(self.temperature)
        frozen_total = QLabel(self.frozen_total)
        f_ice = QLabel(self.f_ice)
        frozen_new = QLabel(self.frozen_new)
        frozen_new_ID = QLabel(self.frozen_new_ID)

        self.leftRightLayout.setVerticalSpacing(20)
        self.leftRightLayout.addRow("temperature: ", temperature)
        self.leftRightLayout.addRow("frozen_total:", frozen_total)
        self.leftRightLayout.addRow("f_ice:", f_ice)
        self.leftRightLayout.addRow("frozen_new:", frozen_new)
        self.leftRightLayout.addRow("frozen_new_ID:", frozen_new_ID)
        self.leftRightLayout.addRow(self.hbox)


    def getPos(self, event):
        x = event.pos().x()
        y = event.pos().y()
        pos = (x, y)
        self.decodePos(x, y, fixPos)

    # from mouse pos -> cell ID
    def decodePos(self, x, y, fixPosition):
        # print(fixPosition)
        # go through all cell in fixpos
        for i in range(len(fixPosition)):
            for j in range(len(fixPosition[i])):
                xmin = fixPosition[i][j][0]
                ymin = fixPosition[i][j][1]
                width = fixPosition[i][j][2]
                height = fixPosition[i][j][3]
                rect = (xmin, ymin, width, height)
                if self.rectangleContains(rect, x, y):
                    result = fixPosition[i][j][8]

                    print("Mouse is clicked at "+str(result))
                    # print(self.image_filename)
                    self.updateResults(result, self.image_filename)

    # check if point is contained within a bounding rectangle
    def rectangleContains(self, rect, x, y):
        result = rect[0] <= x < rect[0] + rect[2] and rect[1] <= y < rect[1] + rect[3]
        return result

    def updateResults(self, id, image_filename):
        global resultsWarning
        # get image number nr
        image_filename_split = image_filename.split("_")
        nr = image_filename_split[-1]
        nr = nr[:-4]
        nr = int(nr)

        # change results value
        for i in range(len(resultsWarning[nr])):
            for j in range(len(resultsWarning[nr][i])):
                if resultsWarning[nr][i][j][8] == id:
                    #0 -> 1
                    if resultsWarning[nr][i][j][4] == 0:
                        result_list = list(resultsWarning[nr][i][j])
                        result_list[4] = 1
                        resultsWarning[nr][i][j] = tuple(result_list)
                        storeResultsWarning(resultsWarning)

                    #1 -> 2
                    elif resultsWarning[nr][i][j][4] == 1:
                        result_list = list(resultsWarning[nr][i][j])
                        result_list[4] = 2
                        resultsWarning[nr][i][j] = tuple(result_list)
                        storeResultsWarning(resultsWarning)
                    # 2 -> 0
                    elif resultsWarning[nr][i][j][4] == 2:
                        result_list = list(resultsWarning[nr][i][j])
                        result_list[4] = 0
                        resultsWarning[nr][i][j] = tuple(result_list)
                        storeResultsWarning(resultsWarning)

        drawBoxUpdate.loadVariables()
        drawBoxUpdate.drawWarningBox(nr)

        # Display Image
        current_dir = os.getcwd()
        src_dir = current_dir + "/result_multi_update_temp/" + os.path.basename(folderPath)
        des_dir = current_dir + "/result_multi_update/" + os.path.basename(folderPath)

        #self.img = QPixmap(src_dir + '/' + self.image_filename)
        self.img = QPixmap(os.path.join(src_dir, self.image_filename))
        self.item_Img.setPixmap(self.img)
        shutil.move(os.path.join(src_dir, self.image_filename), os.path.join(des_dir, self.image_filename))




    # click button Update
    # ban chat la sau khi chay yolo3 thi co results-> bay gio co results updated-> tinh lai cac thu khac
    # update newfrozenID to database and csv and plot ...
    # draw all box prev and till end
    # save new draw and updated images to new folder name_updated trong resultmulti
    # re show GUI = reload this file
    def updateImage(self):
        #da co resultsWarning.data updated chua results(3 class: 0 ,1,2) -> ensure there are only class 0 or 1
        #2 cach: 1 la merge resultsWarning.data vao results.data, 2 la Meldung box yeu cau user k duoc de redbox -> button: clear all warning: nhung pos co warning lay lai value tu results cu

        # kiem tra lai bat dau tu gui yolo multi
        #save image file name
        # ->list image filename tu origin, sau khi click select folder

        #save image path
        #-> list all image path tu origin sau khi click select folder

        #run load:
        # -> PRC_TRAY -> result -> results ( list of all value of all images) ( images _> image -> 8x12 cells -> cell : tupel of 8 elements)

        #cho cac method vao main.......
        ######## get Temperature -> k doi


        #self.calculateVariables()
            #self.frozen_new
            #self.frozen_total
            #self.f_ice.append(temp_frozen / 96)
            #self.frozen_new_ID
                #thay doi chinh
                #tu updated result from resultsWarning -> merged -> results.data
                #neu co frozen moi: 2 TH
                    # frozen cu truoc frozen moi: k xay ra vi da boundary
                    # frozen moi truoc frozen cu: can update: frozen new ID tu pic cu sang pic moi + chuyen class all pic tu pic moi, tinh lai frozen new cac pic, frozen total, fice
                    #xem lai tinh ntn, lieu co reuse lai tu gui yolo multi k

        #########plot chart
            #plot lai
        ######### export CSV
            #export lai + them cot  poisson
        #########CSV to sqlite db3
            #chay lai ->update

        ######### save variables for comparing with old program
            #storeFixPos(self.result) -> k doi

            #storeFolderPath(self.folderPath) -> k doi

            #storeFrozenNewID(self.frozen_new_ID) -> JA

            #storeImagePath(self.imagePath) -> k doi

            #storeResults(self.results) -> JA ( merge resultsWarning _> results)

            #difference: after compare old and new prog

            #newResult: newfrozenID -> compare with old prog

            # resultsWarning, warning box


        ######## frozen cells stay frozen ( boundaryCondition)
            # input : frozen_new_ID, results
            # new ID frozen -> bound (before = 0 and after = 1)-> calculate other value: total, fice, new frozen..
            # output : updated results
            # find image with new frozen ID, from this change all class of rest bild to 1

        ######### run file oldProgram.py -> lieu co can k. su dung lai method compare newID -> compare 3 cap : result_new_prog, result_old_prog, result_reality

        ######### draw bounding box  and save all image to folder: results_multi_update
        # check lai mainGUI chay lai tu dau xem su dung image_folder_name de show and ntn...

        # use this like a tool to generate real world results: -> generate output csv updated file (real world results) and compare with old program / new program /
        pass
        #TODO
        # folien Present + Fragen in 1-2 Woche Marz abgeben, wann Verteidigung/ Quolliquium ?, innerhalb märz
        # TODO
        # 50 50 : generate graph + 2 csv left and right. VD tested 20210217 dominik // GUI chon left and rright -> tinh
        # < 96 : GUI chon pos nao k co -> tinh.
        # Meldung Box, Meldung Tex
        # fix split_name if have
        # entspiegelung fix
        # Doku


def main():
    loadVariables()
    copyResults()
    App = QApplication(sys.argv)
    window = Main()
    sys.exit(App.exec_())


if __name__ == '__main__':
    main()
