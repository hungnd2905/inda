import numpy as np
import cv2
import time


error_input_image_size = 0
error_minimum_detected_objects = 0

# Detecting Objects on Image with YOLO3
# path: path to input image

def yolo3(path):
    global error_input_image_size
    global error_minimum_detected_objects
##### Reading RGB image
    image_BGR = cv2.imread(path)
    print('Image shape:', image_BGR.shape)

    if image_BGR.shape != (960, 1280, 3):
        print("Image size ist not (960,1280)")
        error_input_image_size=1
    h, w = image_BGR.shape[:2]
    print('Image height={0} and width={1}'.format(h, w))

##### Getting Blob format
    blob = cv2.dnn.blobFromImage(image_BGR, 1 / 255.0, (416, 416), swapRB=True, crop=False)
    print('Blob shape:', blob.shape)

##### Loading YOLO v3 network
    with open('yolo-TROPOS-data/classes.names') as f:
        labels = [line.strip() for line in f]
    network = cv2.dnn.readNetFromDarknet('yolo-TROPOS-data/yolov3_INDA_test.cfg', 'yolo-TROPOS-data/yolov3_INDA_train_last.weights')

    #list with names of all layers from YOLO v3 network
    layers_names_all = network.getLayerNames()

    #get indexes of layers with unconnected outputs
    layers_names_output = [layers_names_all[i[0] - 1] for i in network.getUnconnectedOutLayers()]

    #minimum probability to eliminate weak predictions
    probability_minimum = 0.5

    #threshold for filtering weak bounding boxes with non-maximum suppression
    threshold = 0.3

##### Implementing Forward pass
    network.setInput(blob)  # setting blob as input to the network
    start = time.time()
    output_from_network = network.forward(layers_names_output)
    end = time.time()

    # Showing spent time for forward pass
    print('Objects Detection took {:.5f} seconds'.format(end - start))

##### Getting bounding boxes
    #lists for detected bounding boxes,obtained confidences and class's number
    bounding_boxes = []
    confidences = []
    class_numbers = []

    # Going through all output layers after feed forward pass
    for result in output_from_network:
        # Going through all detections from current output layer
        # Every 'detected_objects' numpy array has first 4 numbers with bounding box coordinates and rest 80 with probabilities for every class
        for detected_objects in result:
            # Getting 80 classes' probabilities for current detected object
            scores = detected_objects[5:]
            # Getting index of the class with the maximum value of probability
            class_current = np.argmax(scores)
            # Getting value of probability for defined class
            confidence_current = scores[class_current]

            # Eliminating weak predictions with minimum probability
            if confidence_current > probability_minimum:
                # Scaling bounding box coordinates to the initial image size
                # YOLO data format keeps coordinates for center of bounding box and its current width and height
                box_current = detected_objects[0:4] * np.array([w, h, w, h])

                # get top left corner coordinates from YOLO data format that are x_min and y_min
                x_center, y_center, box_width, box_height = box_current
                x_min = int(x_center - (box_width / 2))
                y_min = int(y_center - (box_height / 2))

                # Adding elements into prepared lists
                bounding_boxes.append([x_min, y_min, int(box_width), int(box_height)])
                confidences.append(float(confidence_current))
                class_numbers.append(class_current)

##### Non-maximum suppression
    #results = detected objects are saved in results
    results = cv2.dnn.NMSBoxes(bounding_boxes, confidences,
                               probability_minimum, threshold)

##### Saving result in list xy_mins
    # defining counter for detected objects
    counter = 1

    # defining counter_wrong for detected objects
    counter_wrong = 0

    # initializing list of tuple xy_mins
    # each tuple contains current bounding box coordinates, its width and height,its class_number and confidences
    # Tuple: (x_min, y_min, box_width, box_height, class_numbers, confidences)
    xy_mins= []

    # Checking if there is at least one detected object after non-maximum suppression
    if len(results) > 0:
        # Going through indexes of results
        for i in results.flatten():
            # get value of x_min, y_min, box_width, box_height
            x_min, y_min = bounding_boxes[i][0], bounding_boxes[i][1]
            box_width, box_height = bounding_boxes[i][2], bounding_boxes[i][3]

            # Showing labels of the detected objects
            print('Object {0}: {1}'.format(counter, labels[int(class_numbers[i])]))

            # Incrementing counter
            counter += 1

            # saving values in list of tuple
            xy_mins.append((x_min, y_min, box_width, box_height, class_numbers[i], confidences[i]))
            print(' ::: x_min, y_min, box_width, box_height {0},{1},{2},{3}'.format(x_min, y_min, box_width, box_height))
            print(' ::: confidences {0}'.format(confidences[i]))

    print('Total objects been detected:', len(bounding_boxes))

    #number of wrong detected objects
    counter_wrong = len(bounding_boxes) - 96
    print('Total wrong objects been detected:', counter_wrong)

##### Removing mirroring of cells at the edges, which means removing wrong detected objects and ensure there are only 96 cells
 #### Firstly remove wrong elements at left and right edges


    #after checking the x_min values, i determine that the difference of two x_min in the same column should < 10
    difference_of_two_x_min = 10

    # Initialize list_of_columns, which consists of all columns from PCR Tray, each columns has 8 elements
    # Each element of column is the tuple (x_min, y_min, box_width, box_height, class_numbers, confidences)
    list_of_columns = []
    # before removing wrong detected objects, ensure that there are mind. 96 elements
    if(len(bounding_boxes) >= 96):
        #sort list of tupel xy_mins by first tupel element x_min
        xy_mins.sort(key=lambda x: x[0])

        # Create list_of_columns
        # x_list consists of all x_min element of xy_mins
        x_list = [x[0] for x in xy_mins]

        #initializing temporary variable temp_list with first element of xy_mins
        temp_list = [xy_mins[0]]

        #go through all elements of x_list
        for t in range(1, len(x_list)):
            # check if difference of two x_min < 10, which means they are in the same column, then append to the temp_list
            if x_list[t] - x_list[t - 1] < difference_of_two_x_min:
                temp_list.append(xy_mins[t])
            # condition to split the column is the difference of two x_min >= 10, which means they are not in the same column, then
            else:
                # check if temp_list has mind. 8 element or not
                # because of mirroring of cells at the edge, in some cases there are extra column with wrong detected cells
                if len(temp_list) >= 8:
                    #add column to the list_of_columns
                    list_of_columns.append(temp_list)
                #initialize temp_list with the current element of xy_mins
                temp_list = [xy_mins[t]]

        #the last column is still not added to the list_of_columns
        #again, remove the case that length of colum < 8
        if len(temp_list) >= 8:
            list_of_columns.append(temp_list)

        #now we should have 12 elements (12 columns in PCR Tray) in list_of_columns
        #each column has at least 8 elements
        print("Number of columns in the PCR Tray:")
        print(len(list_of_columns))
    else:
        #if total objects detected <96 then return error result
        if len(bounding_boxes) < 96:
            print("Error because number objects detected < 96")
            error_minimum_detected_objects = 1

 #### removing wrong elements in top and bottom edges by removing element at column, that has more than 8 elements
    #TODO fix top and colum by comparing distance in y to find which cell to be remove. using len(list_of_columns[i]) == 9 ?. if == 10 -> remove both
    # go through all columns
    for i in range(len(list_of_columns)):
        #x_sum = sum of all x_min value in one column
        x_sum = 0

        #  check the case if the column has 9 elements, which means this column has one reflection on top,
        #  then sort all elements in this column by y_min,
        #  then remove the element with smallest y_min ( top of column)
        if len(list_of_columns[i]) > 8:
            list_of_columns[i].sort(key=lambda x: x[1])
            list_of_columns[i].pop(0)

        # check the case if the column has 10 elements, which means this columns has two reflections on top and bottom
        # then by remove element has biggest y_min ( bottom of column)
        if len(list_of_columns[i]) > 8:
            list_of_columns[i].pop(len(list_of_columns))

        # calculating the average value of x_min for each column of PCR Tray
        # now len(list_of_columns[i]) should be 8
        for j in range(len(list_of_columns[i])):
            x_sum += list_of_columns[i][j][0]
        x_sum = int(x_sum / len(list_of_columns[i]))

        # and then apply this value to all element in this column
        for j in range(len(list_of_columns[i])):
            lst = list(list_of_columns[i][j])
            lst[0] = x_sum
            list_of_columns[i][j] = tuple(lst)
        list_of_columns[i].sort(key=lambda x: x[1])


    # Encode Position
    for i in range(len(list_of_columns)):
        for j in range(len(list_of_columns[i])):
            if j == 0:
                list_of_columns[i][j] += ("A",)
            if j == 1:
                list_of_columns[i][j] += ("B",)
            if j == 2:
                list_of_columns[i][j] += ("C",)
            if j == 3:
                list_of_columns[i][j] += ("D",)
            if j == 4:
                list_of_columns[i][j] += ("E",)
            if j == 5:
                list_of_columns[i][j] += ("F",)
            if j == 6:
                list_of_columns[i][j] += ("G",)
            if j == 7:
                list_of_columns[i][j] += ("H",)

            if i == 0:
                list_of_columns[i][j] += ("1",)
            if i == 1:
                list_of_columns[i][j] += ("2",)
            if i == 2:
                list_of_columns[i][j] += ("3",)
            if i == 3:
                list_of_columns[i][j] += ("4",)
            if i == 4:
                list_of_columns[i][j] += ("5",)
            if i == 5:
                list_of_columns[i][j] += ("6",)
            if i == 6:
                list_of_columns[i][j] += ("7",)
            if i == 7:
                list_of_columns[i][j] += ("8",)
            if i == 8:
                list_of_columns[i][j] += ("9",)
            if i == 9:
                list_of_columns[i][j] += ("10",)
            if i == 10:
                list_of_columns[i][j] += ("11",)
            if i == 11:
                list_of_columns[i][j] += ("12",)
    #Add ID
    for i in range(len(list_of_columns)):
        for j in range(len(list_of_columns[i])):
            row = list_of_columns[i][j][6]
            column = list_of_columns[i][j][7]
            id = ''
            id += row
            id += column
            list_of_columns[i][j] += (id,)

        print("Number of element in each columns of PCR Tray ")
        print(len(list_of_columns[i]))
        print(list_of_columns[i])

    return list_of_columns


def returnError():
    pass
    #if error_input_image_size:
        #error code = 0
        #return 0


    #if error_minimum_detected_objects:
        #error code = 1
        #return 1



