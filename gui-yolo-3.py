import sys
from PyQt5 import QtWidgets

import design

from yolo3 import yolo3

import cv2
from PyQt5 import QtCore
from PyQt5.QtGui import QImage, QPixmap





class MainApp(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.image = None
        self.processedImage = None
        self.loadButton.clicked.connect(self.loadClicked)
        self.folderName = ""

    def loadClicked(self):
        self.fname = QtWidgets.QFileDialog.getOpenFileName(self, 'Choose Image to Open', '.', '*.jpg')
        if self.fname:
            self.loadImage(self.fname)
            print(self.fname[0])
        else:
            print("Image is not selected")
            QtWidgets.qApp.quit()
            return False

    def loadImage(self, fname):
        self.image = cv2.imread(fname[0], cv2.IMREAD_COLOR)
        result = self.processedImage = yolo3(fname[0])
        self.draw_bounding_box(self.image, result)
        result_image = QPixmap('result.jpg')
        origin_image = QPixmap(fname[0])

        self.imgLabel.setPixmap(result_image)
        self.imgLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.processedLabel.setPixmap(origin_image)
        self.processedLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

    # img: input image
    # input: value of list_of_columns
    # GUI interface to load and show image
    def draw_bounding_box(self,img, input):
        for i in range(len(input)):
            for j in range(len(input[i])):
                # class = 0 and confidence >= 0.96
                if input[i][j][4] == 0 and input[i][j][5] >= 0.96:
                    color_box_current = [0, 255, 255]

                # class = 1 and confidence >= 0.96
                if input[i][j][4] == 1 and input[i][j][5] >= 0.96:
                    color_box_current = [255, 0, 255]

                # confidence < 0.96
                if input[i][j][5] < 0.96:
                    # Red
                    color_box_current = [255, 0, 0]

                xmin = input[i][j][0]
                ymin = input[i][j][1]
                boxwidth = input[i][j][2]
                boxheight = input[i][j][3]
                cv2.rectangle(img, (xmin, ymin), (xmin + boxwidth, ymin + boxheight), color_box_current, 2)

        cv2.imwrite("result.jpg", img)

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()
        if e.key() == QtCore.Qt.Key_F11:
            if self.isMaximized():
                self.showNormal()
            else:
                self.showMaximized()

# Defining main function to be run
def main():
    # Initializing instance of Qt Application
    app = QtWidgets.QApplication(sys.argv)

    # Initializing object of designed GUI
    window = MainApp()

    # Showing designed GUI
    window.show()

    # Running application
    app.exec_()


# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()
