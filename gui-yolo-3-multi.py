import csv
import os
import pickle
import shutil
import time

from PyQt5 import QtWidgets
import design

from yolo3 import yolo3


import cv2
from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
from pathlib import Path
import pandas as pd
import sys
import matplotlib.pyplot as plt
import numpy as np
import sqlite3

from storage import storeFixPos
from storage import storeFolderPath
from storage import storeFrozenNewID
from storage import storeImagePath
from storage import storeResults
import collections
import oldProgram
import drawBoundingBox
import boundaryCondition



# Creating GUI interface to load and detect multiple images
class MainApp(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.image = None
        self.processedImage = None
        self.loadButton.clicked.connect(self.loadClicked)

        self.folderPath = ""
        self.PCR_Tray = []
        self.des_dir = ""

        self.result = []
        self.results = []
        self.imagePath = []
        # Data to export
        self.image_filename = []
        self.temperature = []
        self.frozen_total = []
        self.f_ice = []
        self.frozen_new = []
        self.frozen_new_ID = []

    def loadClicked(self):
        self.folderPath = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Directory"))
        #save folderName


        self.showMinimized()

        #if no folder is selected, quit app
        if self.folderPath =="":
            print("Folder is not selected")
            QtWidgets.qApp.quit()
            return False

        print("--------")
        print("Path of input folder:")
        print(self.folderPath)
        print("-------")
        print("Name of folder:")
        print(os.path.basename(self.folderPath))
        print("---------")

        # get the current working dir
        src_dir = os.getcwd()

        # destination directory to save result
        self.des_dir = src_dir + "/result_multi/" + os.path.basename(self.folderPath)

        if os.path.exists(self.des_dir):
            shutil.rmtree(self.des_dir)
            time.sleep(2)
        os.makedirs(self.des_dir)

        print("Path of destination folder:")
        print(self.des_dir)
        print("--------")

        #start folder in window explorer
        os.startfile(self.des_dir)



    ######### read jpg files in selected folder
        for r, d, f in os.walk(self.folderPath):
            for self.file in f:
                if self.file.endswith(".jpg"):
                    self.fname = tuple([os.path.join(r, self.file)] + ['*.jpg'])
                    print("Detected Image:")
                    print(self.fname[0])
                    print(os.path.basename(self.fname[0]))


                    split_fname = os.path.basename(self.fname[0]).split("_")
                    # check if image to be detected belongs to selected folder?
                    # avoid the case wrong images are in selected folder

                    # this method check if a substring is in a string
                    if split_fname[1] in os.path.basename(self.folderPath):
                        #save image file name
                        self.image_filename.append(os.path.basename(self.fname[0]))
                        #save image path
                        self.imagePath.append(self.fname[0])
                        #run YOLO3 Detection
                        self.result = self.loadImage(self.fname)

                        #save detected result
                        self.results.append(self.result)

        self.close()

    ######## get Temperature
        self.getTemperature()

    ####### calculate frozen_total,f_ice, frozen_new, frozen_new_ID
        self.calculateVariables()


    ####### Print results
        self.printResult()


    #########plot chart
        self.plotChart()


    ######### export CSV
        self.exportCSV()


    #########CSV to sqlite db3
        self.CSVtoSqlite3()


    ######### save variables for comparing with old program
        self.storeVariables()

    ######## frozen cells stay frozen

        boundaryCondition.main()

    ######### run file oldProgram.py

        oldProgram.main()

    ######### draw bounding box

        drawBoundingBox.main()
#######################################################################################################

    ##### Detect image with YOLO3
    def loadImage(self, fname):

        self.image = cv2.imread(fname[0], cv2.IMREAD_COLOR)
        temp = yolo3(fname[0])

        #if Picture ist not detected correctly, skip and append last result

        #TODO Test ErrorCode
        #errorCode = returnError()
        #if errorCode == 1:
        if len(temp) < 12:
            self.PCR_Tray.append((self.PCR_Tray[-1]))
            temp = self.PCR_Tray[-1]
            #add error to image_filename
            error_name =self.image_filename[-1]
            error_name ="Error_"+error_name
            self.image_filename[-1] = error_name
        else:
            self.PCR_Tray.append(temp)

        print("begin row 1 of PCR Tray------------------------")
        for i in range(len(temp)):
            print(temp[i])
        print("end row 12 of PCR Tray-------------------------")
        return temp

    def getTemperature(self):
        print("Path of Tlog file:")
        for r, d, f in os.walk(self.folderPath):
            for self.file in f:
                # read csv file
                if self.file.endswith("Tlog.csv"):
                    self.fname_Tlog = os.path.join(r, self.file)
                    print(self.fname_Tlog)
        print("-----------")

        df = pd.read_csv(self.fname_Tlog, sep=";", header=None, usecols=[3])
        self.temperature = df.values.tolist()
        self.temperature = [val for sublist in self.temperature for val in sublist]

    def calculateVariables(self):
        # two dimensional array to mark all gefrozen cells
        old_pos = [[0 for a in range(8)] for b in range(12)]
        for i in range(len(self.PCR_Tray)):
            temp_frozen = 0
            temp_pos_new = []
            frozen_new = 0
            print("begin row 1 of PCR Tray------------------------")
            for j in range(len(self.PCR_Tray[i])):
                print(self.PCR_Tray[i][j])
                for k in range(len(self.PCR_Tray[i][j])):
                    if self.PCR_Tray[i][j][k][4] == 1:
                        if old_pos[j][k] == 0:
                            old_pos[j][k] = 1
                            temp_pos = self.PCR_Tray[i][j][k][6]
                            temp_pos += self.PCR_Tray[i][j][k][7]
                            temp_pos_new.append(temp_pos)
                            frozen_new += 1

            for x in range(len(old_pos)):
                for y in range(len(old_pos[x])):
                    if old_pos[x][y] == 1:
                        temp_frozen += 1
            self.frozen_new.append(frozen_new)
            self.frozen_total.append(temp_frozen)
            self.f_ice.append(temp_frozen / 96)
            self.frozen_new_ID.append(temp_pos_new)

            print("end row 12 of PCR Tray-------------------------")

    def storeVariables(self):
        storeFixPos(self.result)

        storeFolderPath(self.folderPath)

        storeFrozenNewID(self.frozen_new_ID)

        storeImagePath(self.imagePath)

        storeResults(self.results)

    def plotChart(self):
        plt.figure()
        # x : Temperature
        x = self.temperature
        # y:  f_ice
        y = self.f_ice
        # plotting the points
        plt.plot(x, y)
        plt.gca().invert_xaxis()
        # naming the x axis
        plt.xlabel('Temperature in C')
        # naming the y axis
        plt.ylabel('Fraction of frozen samples (f_ice)')

        plt.title('f_ice with YOLO')
        # function to show the plot
        #plt.show()
        plt.savefig(self.des_dir + "/graph.png")

    def exportCSV(self):
        rows = zip(self.image_filename, self.temperature, self.frozen_total,self.f_ice, self.frozen_new, self.frozen_new_ID)
        with open(self.des_dir+"/Output_"+os.path.basename(self.folderPath)+".csv", "w", newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            header = ['image_filename', 'temperature', 'frozen_total', 'f_ice', 'frozen_new', 'frozen_new_ID']
            writer.writerow(header)
            for row in rows:
                writer.writerow(row)

    def CSVtoSqlite3(self):
        con = sqlite3.connect("images2.db")
        cur = con.cursor()
        cur.execute("DROP TABLE IF EXISTS t;")
        cur.execute("CREATE TABLE IF NOT EXISTS t (image_filename,temperature,frozen_total,f_ice,frozen_new,frozen_new_ID);")

        with open(self.des_dir+"/Output_"+os.path.basename(self.folderPath)+".csv", 'r') as fin:
            # csv.DictReader uses first line in file for column headings by default
            dr = csv.DictReader(fin)  # comma is default delimiter
            to_db = [(i['image_filename'], i['temperature'], i['frozen_total'], i['f_ice'],i['frozen_new'],i['frozen_new_ID']) for i in dr]

        cur.executemany("INSERT INTO t (image_filename,temperature,frozen_total,f_ice,frozen_new,frozen_new_ID) VALUES (?, ?, ?, ?, ?, ?);", to_db)
        con.commit()
        con.close()

    def printResult(self):
        print("Number of detected pictures:")
        print(len(self.PCR_Tray))
        print("-------------------------------------------------")
        print("image_filename:")
        print(self.image_filename)
        print("-------------------------------------------------")
        print("length of temperature:")
        print(len(self.temperature))
        print("Temperature from Tlog.csv File: ")
        print(self.temperature)
        print("-------------------------------------------------")
        print("length of frozen_total:")
        print(len(self.frozen_total))
        print("frozen_total:")
        print(self.frozen_total)
        print("--------------------------------------------------")
        print("length of f_ice")
        print(len(self.f_ice))
        print("f_ice:")
        print(self.f_ice)
        print("----------------------------------------------------")
        print("length of frozen_new")
        print(len(self.frozen_new))
        print("frozen_new:")
        print(self.frozen_new)
        print("-----------------------------------------------------")
        print("length of frozen_new_ID")
        print(len(self.frozen_new_ID))
        print("frozen_new_ID:")
        print(self.frozen_new_ID)

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()
        if e.key() == QtCore.Qt.Key_F11:
            if self.isMaximized():
                self.showNormal()
            else:
                self.showMaximized()



# Defining main function to be run
def main():
    # Initializing instance of Qt Application
    app = QtWidgets.QApplication(sys.argv)

    # Initializing object of designed GUI
    window = MainApp()

    # Showing designed GUI
    window.show()

    # Running application
    app.exec_()


# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()
