from pathlib import Path
import pickle
# initialize variables for storage
fixPos = []
folderPath = ""
frozenNewID = []
imagePath = []
results = []
warningBox= []
resultsWarning = []

root = Path(".")

# get variables from gui-yolo3-muti.py
def storeFixPos(input):
    global fixPos
    fixPos = input
    outputFile = 'fixPos.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(fixPos, fw)
    fw.close()

# folder_path
def storeFolderPath(input):
    global folderPath
    folderPath = input
    outputFile = 'folderPath.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(folderPath, fw)
    fw.close()

def storeFrozenNewID(input):
    global frozenNewID
    frozenNewID = input
    outputFile = 'frozenNewID.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(frozenNewID, fw)
    fw.close()

def storeImagePath(input):
    global imagePath
    imagePath = input
    outputFile = 'imagePath.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(imagePath, fw)
    fw.close()

def storeResults(input):
    global results
    results = input
    outputFile = 'results.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(results, fw)
    fw.close()

def storeWarningBox(input):
    global warningBox
    warningBox = input
    outputFile = 'warningBox.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(warningBox, fw)
    fw.close()

def storeResultsWarning(input):
    global resultsWarning
    resultsWarning = input
    outputFile = 'resultsWarning.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(resultsWarning, fw)
    fw.close()




