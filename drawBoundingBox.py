import time
from pathlib import Path
import pickle
import cv2
import shutil
import os
from storage import storeWarningBox
from storage import storeResultsWarning
root = Path(".")

difference = []
imagePath = []
results = []
folderPath = ""
#list of list [i,j,k]:
#i:image i has red pos
# j is column and k row in image i
warningBox =[]
results_Warning = []

def loadVariables():
    global difference
    global imagePath
    global results
    global folderPath
    global results_Warning

    # load difference between old and new program
    inputFile = 'difference.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    difference = pickle.load(fd)

    # load imagePath
    inputFile = 'imagePath.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    imagePath = pickle.load(fd)

    # load results
    inputFile = 'results.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    results = pickle.load(fd)
    results_Warning = results

    # load folderPath
    inputFile = 'folderPath.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    folderPath = pickle.load(fd)

def getRedBox():
    global difference
    global results_Warning
    global warningBox
    # Red box for the difference with old result

    for i in range(len(difference)):
        if difference[i]:
            for x in range(len(difference[i])):
                for j in range(len(results_Warning[i])):
                    for k in range(len(results_Warning[i][j])):
                        if results_Warning[i][j][k][8] == difference[i][x]:
                            temp =[]
                            temp.append(i)
                            temp.append(difference[i][x])

                            #change class to 2
                            warning_list = list(results_Warning[i][j][k])
                            warning_list[4] = 2
                            results_Warning[i][j][k] = tuple(warning_list)
                            warningBox.append(temp)

    for i in range(len(results_Warning)):
        for j in range(len(results_Warning[i])):
            for k in range(len(results_Warning[i][j])):
                if results_Warning[i][j][k][4] == 0 and results_Warning[i][j][k][5] < 0.96:
                    # change class to 2
                    warning_list = list(results_Warning[i][j][k])
                    warning_list[4] = 2
                    results_Warning[i][j][k] = tuple(warning_list)

    return results_Warning

#draw bounding box for one image
def draw_bounding_box(im, input):

    for i in range(len(input)):
        for j in range(len(input[i])):

            # class = 0 (liquid)
            if input[i][j][4] == 0:
                #green
                color_box_current = [0, 255, 255]

            #class = 1  (frozen)
            if input[i][j][4] == 1:
                #blue
                color_box_current = [255, 0, 255]

            #class = 2  (warning)
            if input[i][j][4] == 2:
                #red
                color_box_current = [0, 0, 255]


            xmin = input[i][j][0]
            ymin = input[i][j][1]
            boxwidth = input[i][j][2]
            boxheight = input[i][j][3]

            cv2.rectangle(im, (xmin, ymin), (xmin + boxwidth, ymin + boxheight), color_box_current, 2)


#draw normal bounding box without warning
def drawNormalBox():
    global folderPath
    global imagePath
    for i in range(len(imagePath)):
        img = cv2.imread(imagePath[i], cv2.IMREAD_COLOR)
        draw_bounding_box(img, results_Warning[i])

        # save detected image to destination folder
        src_dir = os.getcwd()
        des_dir = src_dir + "/result_multi/" + os.path.basename(folderPath)
        new_dst_file_name = os.path.join(des_dir, os.path.basename(imagePath[i]))
        cv2.imwrite(new_dst_file_name, img)


# Defining main function to be run
def main():
    loadVariables()
    getRedBox()
    print(warningBox)
    storeWarningBox(warningBox)
    storeResultsWarning(results_Warning)
    drawNormalBox()

# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()
