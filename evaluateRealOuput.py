import pandas as pd
import pickle
from pathlib import Path
import math
import numpy as np
root = Path(".")


def readCSV():

    #colnames = ['# filename', 'T', 'Tcalib', 'ffr', 'Newly Frozen #', 'Newly Frozen IDs','fice_lerr','fice_uerr']
    colnames = ['Image Filename','Temperature','Frozen Count','Frozen Fraction','Newly Frozen #','Newly Frozen IDs']
    input = "INDA_Bilder_updated/20200717_HERA_Jonas_checked/SNOMAX_500nm_40lwdh_n3/output.csv"
    data = pd.read_csv(input, names=colnames)


    # Get column
    newly_frozen_ID = data["Newly Frozen IDs"].tolist()
    newly_frozen_ID.pop(0)
    s = pd.Series(newly_frozen_ID)

    #Convert value

    for i in range(len(s)):
        if(s[i] is np.nan):
            newly_frozen_ID[i] = []
    result = []
    for i in range(len(newly_frozen_ID)):
        if not newly_frozen_ID[i]:
            result.append([])
        if newly_frozen_ID[i]:
            temp_split = str(newly_frozen_ID[i]).split(",")
            for i in range(len(temp_split)):
                temp_split[i] = temp_split[i].strip()
            result.append(list(temp_split))

    print(result)



    #save result

    realResult = result
    outputFile = 'realResult.data'
    outputPath = root / "Data" / outputFile
    fw = open(outputPath, 'wb')
    pickle.dump(realResult, fw)
    fw.close()


    #print(newly_frozen_ID)
# Defining main function to be run
def main():
    readCSV()

# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()