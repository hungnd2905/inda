import os
import shutil
import time

import pickle
from pathlib import Path
import cv2

root = Path(".")
resultsWarning = []
folderPath = ""
imagePath = []

def loadVariables():
    global resultsWarning
    global folderPath
    global imagePath

    # load imagePath
    inputFile = 'resultsWarning.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    resultsWarning = pickle.load(fd)

    # load folderPath
    inputFile = 'folderPath.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    folderPath = pickle.load(fd)

    # load imagePath
    inputFile = 'imagePath.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    imagePath = pickle.load(fd)

def drawWarningBox(imageNr):
    global folderPath
    global imagePath

    img = cv2.imread(imagePath[imageNr], cv2.IMREAD_COLOR)
    draw_bounding_box(img, resultsWarning[imageNr])

    # save detected image to destination folder
    src_dir = os.getcwd()
    des_dir = src_dir + "/result_multi_update_temp/" + os.path.basename(folderPath)

    # create folder
    if os.path.exists(des_dir):
        shutil.rmtree(des_dir)
        time.sleep(0.1)
    os.makedirs(des_dir)

    new_dst_file_name = os.path.join(des_dir, os.path.basename(imagePath[imageNr]))

    cv2.imwrite(new_dst_file_name, img)

def draw_bounding_box(im, input):

    for i in range(len(input)):
        for j in range(len(input[i])):

            # class = 0 (liquid)
            if input[i][j][4] == 0:
                #green
                color_box_current = [0, 255, 255]

            #class = 1  (frozen)
            if input[i][j][4] == 1:
                #blue
                color_box_current = [255, 0, 255]

            #class = 2  (warning)
            if input[i][j][4] == 2:
                #red
                color_box_current = [0, 0, 255]


            xmin = input[i][j][0]
            ymin = input[i][j][1]
            boxwidth = input[i][j][2]
            boxheight = input[i][j][3]

            cv2.rectangle(im, (xmin, ymin), (xmin + boxwidth, ymin + boxheight), color_box_current, 2)





# after UPDATE clicked, draw the rest
def drawAfterUpdate():
    pass


# Defining main function to be run
def main():
    pass

# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()
