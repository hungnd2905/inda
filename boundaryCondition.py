import pickle
from pathlib import Path
from storage import storeResults
results = []
frozen_new_ID = []

root = Path(".")


def loadVariables():
    global frozen_new_ID
    global results

    # load results
    inputFile = 'results.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    results = pickle.load(fd)

    #load frozenNewID
    inputFile = 'frozenNewID.data'
    inputPath = root / "Data" / inputFile
    fd = open(inputPath, 'rb')
    frozen_new_ID= pickle.load(fd)



#Ensure that frozen cells stay frozen
#First Boundary after get Result from YOLO
def boundaryCondition():
    global results
    for t in range(len(frozen_new_ID)):
        #get the new frozen id of each image
        temp_frozen_ID = frozen_new_ID[t]

        if temp_frozen_ID:
            print("new frozen ID found, frozen cells stay frozen since image.. ")
            print(t)
            print("new frozen ID list:")
            print(temp_frozen_ID)

            #for each new frozen ID:
            for h in range(len(temp_frozen_ID)):
                #for all images from index (t +1) to the end,...
                for i in range(t+1,len(results)):
                    #go through all 96 position of PCR Tray
                    for j in range(len(results[i])):
                        for k in range(len(results[i][j])):
                            #check if Id of each cell == new frozen ID?
                            if (results[i][j][k][8] == temp_frozen_ID[h]):
                                print("----------")
                                print("Image Nr. is changed:")
                                print(i)
                                print("ID:")
                                print(temp_frozen_ID[h])
                                print("before changing class to 1 (frozen)")
                                print(results[i][j][k])
                                results_list= list(results[i][j][k])
                                results_list[4] =1
                                results[i][j][k] =tuple(results_list)
                                print("after changing class to 1")
                                print(results[i][j][k])
            print("-----------------------------------------------------------")
    storeResults(results)

#TODO tu yolo multi get -> new ID frozen -> bound (before = 0 and after = 1)-> calculate other value: total, fice, new frozen..
#TODO Boundary with input (new frozen ID index ), prev = 0, since = 1

# Defining main function to be run
def main():
    loadVariables()
    boundaryCondition()
# Checking if current namespace is main, that is file is not imported
if __name__ == '__main__':
    # Implementing main() function
    main()
